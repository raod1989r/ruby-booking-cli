class Booking
  @@service_tax = 14
  @@swach_tax = 0.5
  @@krishi_kalyan_tax = 0.5

  def self.service_tax
    @@service_tax
  end

  def self.swach_tax
    @@swach_tax
  end

  def self.krishi_kalyan_tax
    @@krishi_kalyan_tax
  end

  def self.run
    Seat.available_seats_in_all_shows
    self.main_menu
  end

  def self.main_menu
    puts "\n\n\n"
    puts "1. Book the seat by entering one"
    puts "2. Total Revenue"
    puts "3. Close Application"
    puts "4. List All seats from available shows"

    input = gets.chomp

    case input
    when '1'
      shows = Show.all

      self.book_a_seat shows
    when '2'
      self.print_complete_bill
    when '3'
      puts 'Closing application...'
      return
    when '4'
      Seat.available_seats_in_all_shows
    else
      puts 'Enter valid input'
      main_menu
    end
  end

  def self.book_a_seat shows
    shows_names = shows.map{|s| "#{s.id} for #{s.name}"}.join(', ')
    puts "Enter show ID - #{shows_names}"

    show_id = gets.chomp
    show = Show.find show_id.to_i

    if !show.nil?
      puts "Enter booking seats"
      seats_selected = gets.chomp.upcase.split(',').map(&:strip)
      seats = show.get_available_seats(seats_selected)

      if seats.all?
        seats.map do |seat|
          show.block_seat seat
          show.confirm_book_seat seat
        end
        puts "Successfully booked - #{show.name}"

        print_receipt_booking seats
        main_menu
      else
        puts "Selected Seats #{(seats_selected - seats.compact.map(&:seat_code)).join(', ')} already sold, Please try again"
      end
    else
      puts "Not valid show with entered name"
    end

    puts "\n"
    puts "1. Go to main menu"
    puts "2. Re-enter show ID"

    case gets.chomp
    when '1' then main_menu
    when '2' then book_a_seat shows
    else
      puts 'Enter valid input'
    end
  end

  def self.print_complete_bill
    puts "Total Sales"
    seats = Seat.where status: Seat::STATUS[:not_available]
    revenue = seats.map{|seat| seat.category.price}.inject(:+)
    puts "Revenue: Rs. #{revenue || 0}"

    service_tax = (revenue.to_f * Booking.service_tax)/100
    puts "Service Tax: Rs. #{service_tax}"

    swach_tax = (revenue.to_f * Booking.swach_tax)/100
    puts "Swachh Bharat Cess: Rs. #{swach_tax}"

    kisan_tax = (revenue.to_f * Booking.krishi_kalyan_tax)/100
    puts "Krishi Kalyan Cess: Rs. #{kisan_tax}"

    main_menu
  end

  def self.print_receipt_booking seats
    sub_total = seats.map{|seat| seat.category.price}.inject(:+)
    puts "SubTotal: Rs. #{sub_total}"

    service_tax = (sub_total.to_f * Booking.service_tax)/100
    puts "Service Tax @#{Booking.service_tax}%: Rs. #{service_tax}"

    swach_tax = (sub_total.to_f * Booking.swach_tax)/100
    puts "Service Tax @#{Booking.swach_tax}%: Rs. #{swach_tax}"

    kisan_tax = (sub_total.to_f * Booking.krishi_kalyan_tax)/100
    puts "Service Tax @#{Booking.krishi_kalyan_tax}%: Rs. #{kisan_tax}"

    puts "Total: Rs. #{sub_total + service_tax + swach_tax + kisan_tax}"
  end
end
