require './models/base.rb'
require './models/seat.rb'
require './models/seat_category.rb'
require './models/show.rb'
require './utils/name.rb'
require './services/booking.rb'
require './seed.rb'

class String
  def red;            "\033[31m#{self}\033[0m" end
  def green;          "\033[32m#{self}\033[0m" end
end

[Show, SeatCategory, Seat].each{|k| k.send(:extend, Name)}

seed_data do
  Booking.run
end
