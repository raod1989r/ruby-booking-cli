module Name
  def self.extended base
    base_name = base.to_s

    extension = Module.new
    extension.send(:define_method, :model_name) do
      base_name
    end

    base.send(:extend, extension)
  end
end
