class Seat < Base
  attr_accessor :seat_code, :category_id, :show_id, :status
  STATUS = {available: :available, blocked: :blocked, not_available: :not_available}

  def category
    SeatCategory.find category_id
  end

  def show
    Show.find show_id
  end

  def self.print_available_seat_layout show
    results = show.all_seats.group_by {|s| s.category.name}

    results.each do |category, seats|
      puts "(#{category.to_s.upcase} class - price (Rs. #{seats[0].category.price}))"
      seats.each do |seat|
        if seat.status.eql? STATUS[:available]
          print "#{seat.seat_code} ".green
        else
          print "#{seat.seat_code} ".red
        end
      end
      puts "\n"
    end
  end

  def self.available_seats_in_all_shows
    puts "Seating Arrangement"
    Show.all.each do |show|
      puts "\n"
      puts "#{show.name} Running in Audi #{show.id}"
      puts "All Seats:"
      self.print_available_seat_layout show
    end
  end
end
