class Show < Base
  attr_accessor :name

  def available
    Seat.where status: Seat::STATUS[:available], show_id: self.id
  end

  def not_available
    Seat.where status: Seat::STATUS[:not_available], show_id: self.id
  end

  def blocked
    Seat.where status: Seat::STATUS[:blocked], show_id: self.id
  end

  def all_seats
    Seat.where show_id: self.id
  end

  def block_seat seat
    if !seat.nil? && seat.status.eql?(Seat::STATUS[:available])
      seat.status = Seat::STATUS[:blocked]
    else
      puts "Not available to book #{seat_code}"
      puts "#{self.available.map(&:seat_code)} - Available Seats"
    end
  end

  def confirm_book_seat seat
    if !seat.nil? && seat.status.eql?(Seat::STATUS[:blocked])
      seat.status = Seat::STATUS[:not_available]
    else
      puts "Not available to book #{seat_code}"
      puts "#{self.available.map(&:seat_code)} - Available Seats"
    end
  end

  def check_for_available_seats seat_codes
    get_available_seats(seat_codes).all?
  end

  def get_available_seats seat_codes
    seat_codes.map do |seat_code|
      Seat.find_by status: Seat::STATUS[:available], seat_code: seat_code
    end
  end
end
