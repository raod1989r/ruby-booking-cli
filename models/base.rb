class Base
  @@all = {}
  @@id = {}
  attr_accessor :id

  def initialize attrs = {}
    @@all[self.class.to_s] = [] if @@all[self.class.to_s].nil?
    @@id[self.class.to_s] = 0 if @@id[self.class.to_s].nil?

    @@id[self.class.to_s] = self.class.class_id + 1
    self.id = self.class.class_id

    attrs.each do |key, value|
      self.send("#{key}=", value)
    end

    @@all[self.class.to_s].push self
  end

  def self.class_id
    @@id[self.model_name]
  end

  def self.id
    @@id[self.model_name]
  end

  def self.all
    @@all[self.model_name]
  end

  def self.find id
    self.all.select{|c| c.id.eql?(id)}[0]
  end

  def self.filter_conditions attrs
    finder_keys = attrs.keys

    self.all.select do |c|
      result = finder_keys.map do |key|
        c.send(key).eql?(attrs[key])
      end

      result.all?
    end
  end

  def self.find_by attrs
    results = self.filter_conditions attrs

    results[0]
  end

  def self.where attrs
    self.filter_conditions attrs
  end
end
