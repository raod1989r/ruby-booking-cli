def seed_data &block
  shows_data = [{name: 'Show1', gold: 25, silver: 34, ordinary: 50},
               {name: 'Show2', gold: 20, silver: 25, ordinary: 40},
               {name: 'Show3', gold: 10, silver: 15, ordinary: 25}
              ]

  {'gold': 250, 'silver': 150, 'ordinary': 100}.each do |key, value|
    SeatCategory.new name: key, price: value
  end

  shows_data.each do |show_details|
    show = Show.new name: show_details[:name]

  # Gold Seats
    show_details[:gold].times.each do |num|
      Seat.new seat_code: "A#{num+1}", category_id: 1, show_id: show.id, status: Seat::STATUS[:available]
    end

  # Silver Seats
    show_details[:silver].times.each do |num|
      Seat.new seat_code: "B#{num+1}", category_id: 2, show_id: show.id, status: Seat::STATUS[:available]
    end

  # Ordinary Seats
    show_details[:ordinary].times.each do |num|
      Seat.new seat_code: "C#{num+1}", category_id: 3, show_id: show.id, status: Seat::STATUS[:available]
    end
  end

  block.call
end
